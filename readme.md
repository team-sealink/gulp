# Sealink gulp js configuration #

http://gulpjs.com/

Dont use ```npm install -g ``` flags
Instead reference in package.json under "scripts" then you can run ```npm run task```


## Install Notes ##
Copy gulpfile.js and package.json to root of project

Create any needed directories:
mkdir -p public_html/assets/coffee/ && mkdir public_html/assets/img/ && mkdir public_html/assets/scss/ && touch public_html/assets/scss/app.scss || echo "app.scss exists"

Install Gulp Packages:
npm install
